﻿using Arch.Core;
using Arch.Core.Enums;
using Arch.Data.UnitofWork;
using Arch.Dto.ParamDto;
using Arch.Dto.SingleDto;
using Arch.Service.Interfaces;
using Arch.Web.Framework.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using Arch.Core.Constants;
using Arch.Utilities.Manager;

namespace Arch.Web.Controllers
{
    public class PersonController: AdminController
    {
        private readonly IWorkService _workService;
        private readonly IPersonService _personService;
        private readonly IMediaService _mediaService;
        private readonly IUtilityService _utiltiyService;
        public PersonController(
            IWorkService workService,
            IUnitofWork uow,
            IPersonService personService,
            IUtilityService utiltiyService,
            IMediaService mediaService,
            ILogService logService)
            : base(uow, logService)
        {
            _workService = workService;
            _personService = personService;
            _mediaService = mediaService;
            _utiltiyService = utiltiyService;
        }

        public ActionResult Edit(int id)
        {
            var data = _personService.FindPerson(id);
            ViewBag.pass = EnDeCode.Decrypt(data.Password, StaticParams.SifrelemeParametresi);
            return View(data);
        }
        
        //[HttpPost]
        //public ActionResult Edit(Person person)
        //{
        //    var data = _personService.FindPerson(id);
        //    ViewBag.pass = EnDeCode.Decrypt(data.Password, StaticParams.SifrelemeParametresi);
        //    return View(data);
        //}

       
        
    }
}